import { Link } from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';

const Page02 = () => {

    const nome = useSelector(state => state.AlunoReducer.nome); 

    const dispatch = useDispatch();

    const handleName = () => {
        dispatch({
            type: 'SET_NAME',
            payload: {
                nome: 'Welleson'
            }
        })
    }

    return (
        <div>
            Page 02
            Bem-vindo, {nome}!
            <button onClick={handleName}>Alterar</button>
            <Link to='/page01'>Tela 01</Link>
        </div>
    )
}

export default Page02;