import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Page01 = () => {
    
    const nome = useSelector(state => state.AlunoReducer.nome);

    return (
        <div>
            <p>Page 01</p>
            <p>Nome: {nome}!</p>
            <Link to="/page02">Tela 02</Link>
        </div>
    )
}

export default Page01;