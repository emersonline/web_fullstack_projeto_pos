import { combineReducers } from 'redux';

import AlunoReducer from './AlunoReducer';

export default combineReducers({
    AlunoReducer
})