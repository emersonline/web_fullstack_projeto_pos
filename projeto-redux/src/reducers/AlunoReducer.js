const initialState = {
    nome: 'Emerson'
}

const AlunoReducer = (state = initialState, action) => {

    switch(action.type){
        case "SET_NAME":
            return {
                ...state, nome: action.payload.nome 
            };
            break;
        default:
            break;
    }

    return state;
}

export default AlunoReducer;