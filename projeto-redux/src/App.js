import { BrowserRouter, Switch, Route} from 'react-router-dom';

import Page01 from './pages/Page01';
import Page02 from './pages/Page02';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact={true} component={ () => <h1>RAIZ DO PROJETO</h1> } />
        <Route path="/page01" component={Page01} />
        <Route path="/page02" component={Page02} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;