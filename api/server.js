const express = require('express');
const mongoose = require('mongoose');

const AlunoSchema = require('./schemas/AlunoSchema');

const server = express();

server.use(express.json());

mongoose.connect('mongodb+srv://admin:admin@cluster0.xmg7m1u.mongodb.net/posweb?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true 
});

//Rota para inserir aluno 
server.post('/aluno', async (req, res) => {
    
    // const { nome, matricula, disciplina } = req.body;
    const nome = req.body.nome;
    const matricula = req.body.matricula;
    const disciplina = req.body.disciplina;

    const aluno = await AlunoSchema.create({nome: nome, matricula: matricula, disciplina: disciplina});

    res.status(201).json(aluno);
});

// Rota para listar alunos
server.get('/aluno', async (request, response) => {
    const alunos = await AlunoSchema.find();

    return response.status(201).json(alunos);
});

// Rota de atualização de registro
server.put('/aluno/:id', async (request, response) => {
    const { id } = request.params;

    // console.log(id);
    const aluno = await AlunoSchema.updateOne({'_id_':id}, request.body);

    return response.status(200).json(aluno); 
})

//DELETANDO
server.delete('/aluno/:id', async(request, response) => {
    const { id } = request.params;

    const aluno = await AlunoSchema.deleteOne({'_id':id});

    return response.status(200).json('Registro exluído!'); 
});

//Pegando registro por dados específicos
server.get('/aluno/:id', async(request, response) => {
    const { id } = request.params;

    const aluno = await AlunoSchema.findById(id);

    return response.status(200).json(aluno); 
});


server.listen(3003,
    () => console.log('Servidor iniciado na porta http:://localhost:3003'))
