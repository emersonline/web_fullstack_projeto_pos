import { useState } from 'react';
import './styles.css';

const Home = () => {
    const [conta, setConta] = useState(0);
    const [porcentagem, setPorcentagem] = useState(0);
    const [resultado, setResultado] = useState(0);

    const calcular = () => {
        let calculo = Number(conta) * Number(porcentagem) / 100;
        setResultado(calculo);
    }

    return(
        <div className="container">
            <img src="https://s3.sa-east-1.amazonaws.com/blog.adrua.com/blog/wp-content/uploads/2018/09/20200803/gar%C3%A7om.jpg" width="600"/>

            <div>
                <h1>Calculadora de Gorjeta</h1>

                <h2>Digite o valor da conta</h2>
                <input placeholder='Ex.: 5.00' onChange={ (txt) => setConta(txt.target.value) }/>

                <h2>Selecione a porcentagem da conta</h2>
                <select onChange={ (txt) => setPorcentagem(txt.target.value) }>
                    <option>Selecione</option>
                    <option value="5">5%</option>
                    <option value="10">10%</option>
                    <option value="15">15%</option>
                </select>
                <br/>
                <br/>

                <button onClick={calcular}>Calcular</button>

                {resultado === 0 ? null : <h2>Resultado: {resultado}</h2>}
            </div>
        </div>
    )
}

export default Home;