import { useParams } from "react-router-dom";

const Details = () => {

    const parametros = useParams();

    const nome = parametros.nome;

    return(
        <div>
            <h1>Bem-vindo, {nome}!</h1>
        </div>
    )
}

export default Details;