import { useState } from 'react';
import {Link} from 'react-router-dom';

const Home = () => {
    const [nome, setNome] = useState('');

    return(
        <div>
            <h1>Página Home - {nome}</h1>

            <input placeholder='Digite seu nome' onChange={ (txt) => setNome(txt.target.value) }></input>

            <br/><Link to={`/details/${nome}`}>Ver página details</Link>
            <br/><Link to={`/gorjeta`}>Ver página Gorjeta</Link>
        </div>
    )
}

export default Home;