import { BrowserRouter, Switch, Route} from 'react-router-dom';

import Home from './Home';
import Details from './Details';
import Gorjeta from './Gorjeta';

const App = () => {
  return(
      <BrowserRouter>
        <Switch>
          <Route path="/" exact={true} component={ () => <h1>RAIZ DO PROJETO</h1> } />
          <Route path="/home" component={Home} />
          <Route path="/details/:nome" component={Details} />
          <Route path="/gorjeta" component={Gorjeta} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;