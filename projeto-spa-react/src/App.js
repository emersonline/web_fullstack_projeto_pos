import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Contato from './pages/Contato';
import Services from './pages/Services';

import Header from './components/Header';
import Footer from './components/Footer';

import './style.css';

const Routes = () => {
  return(
    <BrowserRouter>
      <Header />
      <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/services" component={Services} />
          <Route path="/contato" component={Contato} />
      </Switch>
      <Footer />
    </BrowserRouter>
  )
}

export default Routes;