import Logo from '../../img/logo.svg';

import './styles.css';

const Home = () => {
    return (
        <main className="container-home">
            <img src={Logo} alt="logo"/>

            <div>
                <h1>Meu Título</h1>
                <p>Meu site feito em react </p>
                <button>Acessar Serviços</button>
            </div>
        </main>
    )
}

export default Home;
