import Service from '../../components/Service';

import './styles.css';

const Services = () => {
    return (
        <div className="container-services">
            <Service name="Serviço de Limpeza"/>
            <Service name="Serviço de Escritório"/>
            <Service name="Serviço de RH"/>
            <Service name="Serviço de TI"/>
            <Service name="Serviço de Construção"/>
            <Service />
        </div>
    )
}

export default Services;
