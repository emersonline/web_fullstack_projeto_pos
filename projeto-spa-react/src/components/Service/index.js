import image from '../../img/logo.svg';

import './style.css';

const Service = ({name = "Serviço não definido"}) => {
    return (
        <div className="container-service">
            <img src={image} alt="Imagem Service"/>
            <p>{name}</p>
        </div>
    )
}

export default Service;